/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_CALOFUTUREINTERFACESDICT_H
#define DICT_CALOFUTUREINTERFACESDICT_H 1

#include "CaloFutureInterfaces/ICaloFuture2CaloFuture.h"
#include "CaloFutureInterfaces/ICaloFuture2MCTool.h"
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"
#include "CaloFutureInterfaces/ICaloFutureClusterTool.h"
#include "CaloFutureInterfaces/ICaloFutureClusterization.h"
#include "CaloFutureInterfaces/ICaloFutureCorrection.h"
#include "CaloFutureInterfaces/ICaloFutureDigitTool.h"
#include "CaloFutureInterfaces/ICaloFutureGetterTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureInterfaces/ICaloFutureHypoLikelihood.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureInterfaces/ICaloFutureLikelihood.h"
#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"
#include "CaloFutureInterfaces/ICaloFutureSplitTool.h"
#include "CaloFutureInterfaces/ICaloFutureSubClusterTag.h"
#include "CaloFutureInterfaces/ICaloFutureTrackIdEval.h"
#include "CaloFutureInterfaces/ICaloFutureTrackMatch.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "CaloFutureInterfaces/IPart2CaloFuture.h"
#include "CaloFutureInterfaces/ITrack2CaloFuture.h"

#endif // DICT_CALOFUTUREINTERFACESDICT_H
