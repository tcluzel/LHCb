<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!DOCTYPE gdd SYSTEM "gdd.dtd" >
<gdd>
  <package name="RecEvent">

<!-- **************************************************************************
  *****************************************************************************
  * XML-description of MuonPID class                                          *
  * author: Erica Polycarpo                                                   *
  * date:   2006-01-13                                                        *
  ************************************************************************* -->

    <class name="MuonPID"
      author="M. Gandelman, E. Polycarpo"
      desc="Stores the output of the Muon Identification in the muon system"
      final = "TRUE"
      defaultdestructor="FALSE"
      id="11050" >
      <base name="KeyedObject&lt;int&gt;"/>
      &KeyedObject;
      <import name="Event/Track"/>

      <constructor 
	desc     = "Copy constructor. Creates a new MuonPID object with the same pid information" 
  initList = "KeyedObject&lt;int&gt;(), m_MuonLLMu(lhs.m_MuonLLMu), m_MuonLLBg(lhs.m_MuonLLBg), m_NShared(lhs.m_NShared), m_Status(lhs.m_Status), m_chi2Corr(lhs.m_chi2Corr), m_muonMVA1(lhs.m_muonMVA1), m_muonMVA2(lhs.m_muonMVA2), m_muonMVA3(lhs.m_muonMVA3), m_muonMVA4(lhs.m_muonMVA4), m_IDTrack(lhs.m_IDTrack), m_muonTrack(lhs.m_muonTrack)">
  <arg const="TRUE" name="lhs" type="LHCb::MuonPID"/>
      	</constructor>

      <attribute type="double" name="MuonLLMu"
        desc="Muon Likelihood for muons" init="-20.0"/>

      <attribute type="double" name="MuonLLBg"
        desc="Muon likelihood for non muons" init="0.0"/>

      <attribute type="int" name="NShared"
        desc="Number of tracks which share hits" init="0"/>

      <attribute type="bitfield" name="Status" desc="Status of MuonPID">
        <bitfield name="IsMuon"         length="1" desc="Boolean: Is track a muon ?"/>
        <bitfield name="InAcceptance"   length="1" desc="Boolean: True if track extraoplation is in the Muon acceptance"/>
        <bitfield name="PreSelMomentum" length="1" desc="Boolean: True if track has minimal momentum"/>
      	<bitfield name="IsMuonLoose"    length="1" desc="Boolean: Is track a muon (looser defintion) ?"/>
	      <bitfield name="IsMuonTight"    length="1" desc="Boolean: Is track a muon (with x,y crossing requirement for muon hits) ?"/>
      </attribute>

      <attribute type="float" name="chi2Corr"
        desc="chi2 using correlations of the hits" init="0.0"/>

      <attribute type="float" name="muonMVA1"
        desc="multi variate algorithm for MuonID with tune1" init="-999.0"/>

      <attribute type="float" name="muonMVA2"
        desc="multi variate algorithm for MuonID with tune2" init="-999.0"/>

      <attribute type="float" name="muonMVA3"
        desc="multi variate algorithm for MuonID with tune3" init="-999.0"/>

      <attribute type="float" name="muonMVA4"
        desc="multi variate algorithm for MuonID with tune4" init="-999.0"/> 

      <relation type="LHCb::Track" name="IDTrack"
        desc="The track that has been IDed by the Muon system"/>
    
      <relation type="LHCb::Track" name="muonTrack"
        desc="The track segment as reconstructed in the Muon system"/>

      <location name="Default" place="Rec/Muon/MuonPID"/>
      <location name="Offline" place="Rec/Muon/MuonPID"/>
      <location name="Hlt"     place="Hlt/Muon/MuonPID"/>

    </class>
  </package>
</gdd>
